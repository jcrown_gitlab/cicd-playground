
|         filepath         | passed | failed | error | skipped | xfailed | xpassed | SUBTOTAL |
| ------------------------ | -----: | -----: | ----: | ------: | ------: | ------: | -------: |
| examples/test_error.py   |        |        |     2 |         |         |         |        2 |
| examples/test_failed.py  |        |      2 |       |         |         |         |        2 |
| examples/test_pass.py    |      2 |        |       |         |         |         |        2 |
| examples/test_skipped.py |        |        |       |       2 |         |         |        2 |
| examples/test_xfailed.py |        |        |       |         |       2 |         |        2 |
| examples/test_xpassed.py |        |        |       |         |         |       2 |        2 |
| TOTAL                    |      2 |      2 |     2 |       2 |       2 |       2 |       1

